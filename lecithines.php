<?php
include ("header.php");
?>

<section id="about" class="ls section_padding_top_25">

    <div class="container">
        <div class="row">

            <div class="col-sm-7 col-lg-7  text-left">
                <h3> LÉCITHINES (E322)</h3>
                <p>En tant qu'émulsifiant, la lécithine de soja est utilisée dans les applications alimentaires comme agent d'aération, régulateur de viscosité, dispersant et lubrifiant.
Typiquement, une émulsion est une suspension de petites gouttelettes d'un liquide dans un autre liquide avec lequel elle est incapable de se mélanger : Huile dans eau (O/W) et eau dans huile (W/O) sont les deux principaux types d'émulsions.
                </p>
                <p>La structure moléculaire de la lécithine lui confère les caractéristiques d’un émulsifiant efficace pour l’interaction de l’eau et l’huile. Les phospholipides, le composant principal de la lécithine, sont en partie hydrophiles (attirés par l'eau) et en partie hydrophobes (repoussés par l'eau). C’est la capacité de la lécithine à interagir simultanément avec l’huile et l’eau qui en fait un émulsifiant efficace et stable.</p>
                <p>Lorsqu'il est introduit dans un système, un émulsifiant tel que la lécithine contribue à maintenir une émulsion stable entre deux liquides non miscibles. L'émulsifiant diminue la tension superficielle entre les deux liquides et leur permet de se mélanger et de former une dispersion hétérogène stable.</p>
            </div>

            <div class="col-sm-5 col-lg-5  text-left">
                <div class="owl-carousel owl-theme" id="product_carousel">
                    <div class="item">
                        <article class="vertical-item content-padding post format-standard with_background rounded">
                            <div class="item-media">
                                <img src="files/img/LECITHINES/1P.jpg" alt="">
                                <div class="media-links">
                                    <div class="links-wrap">
                                        <a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]"
                                            href="files/img/LECITHINES/1P.jpg"></a>
                                    </div>
                                </div>
                            </div>

                        </article>
                    </div>
                    <div class="item">
                        <article class="vertical-item content-padding post format-standard with_background rounded">
                            <div class="item-media">
                                <img src="files/img/LECITHINES/2P.jpg" alt="">
                                <div class="media-links">
                                    <div class="links-wrap">
                                        <a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]"
                                            href="files/img/LECITHINES/2P.jpg"></a>
                                    </div>
                                </div>
                            </div>

                        </article>
                    </div>
                    <div class="item">
                        <article class="vertical-item content-padding post format-standard with_background rounded">
                            <div class="item-media">
                                <img src="files/img/LECITHINES/3P.jpg" alt="">
                                <div class="media-links">
                                    <div class="links-wrap">
                                        <a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]"
                                            href="files/img/LECITHINES/3P.jpg"></a>
                                    </div>
                                </div>
                            </div>

                        </article>
                    </div>
                    <div class="item">
                        <article class="vertical-item content-padding post format-standard with_background rounded">
                            <div class="item-media">
                                <img src="files/img/LECITHINES/4P.jpg" alt="">
                                <div class="media-links">
                                    <div class="links-wrap">
                                        <a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]"
                                            href="files/img/LECITHINES/4P.jpg"></a>
                                    </div>
                                </div>
                            </div>

                        </article>
                    </div>
                    <div class="item">
                        <article class="vertical-item content-padding post format-standard with_background rounded">
                            <div class="item-media">
                                <img src="files/img/LECITHINES/5P.jpg" alt="">
                                <div class="media-links">
                                    <div class="links-wrap">
                                        <a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]"
                                            href="files/img/LECITHINES/5P.jpg"></a>
                                    </div>
                                </div>
                            </div>

                        </article>
                    </div>
                    
                </div>

            </div>

        </div>
        <div class="row" style="margin-bottom:50px;">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <p>
                    <strong>
                    Principales utilisations :


                    </strong>
                <ul>
                    <li>Chocolats et confiserie</li>
                    <li>Margarines</li>
                    <li>Boulangerie</li>
                    <li>Boissons instantanées</li>
                    <li>Formulation de peintures (non food)</li>
                    <li>...</li>

                </ul>
                <strong>
                En pratique, les dosages typiques de la lécithine dans une émulsion varient comme suit :




                    </strong>
                <ul>
                    <li>1-5% de la MG  pour un système W/O</li>
                    <li>5-10% de la MG  pour O/W</li>
                

                </ul>
                <p>La quantité de lécithine utilisée dépend de facteurs tels que le HLB,  pH, l'inclusion de protéines et  gommes et la concentration en sel.

</p>
                </p>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                
                <strong>
                Les principales références commercialisées par INGREDIS TUNISIE sont:
                </strong>
                <ul>
                    <li>Lécithine de soja (VEROLEC).</li>
                    <li>Lécithine de soja NON OGM IP.</li>
                    <li>Lécithine de tournesol (GIRALEC).</li>
                    <li>Lécithine de colza.</li>
                    <li>Farine lécithinée pour la boulangerie (LECISOL).</li>
                    <li>Lécithine pour boisson instantanée en poudre (poudre chocolatée).</li>
                    <li>Lécithine en Poudre</li>
                    <li>...</li>

                </ul>
            </div>
        </div>
    </div>
</section>
</div>




<script>
$("#product_carousel").owlCarousel({

    navigation: true, // Show next and prev buttons

    slideSpeed: 300,
    paginationSpeed: 400,

    items: 1,
    itemsDesktop: false,
    itemsDesktopSmall: false,
    itemsTablet: false,
    itemsMobile: false

});
</script>

<?php
include ("footer.php");
?>