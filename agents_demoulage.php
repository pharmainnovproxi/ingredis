<?php
include ("header.php");
?>

<section id="about" class="ls section_padding_top_25">

    <div class="container">
        <div class="row">

            <div class="col-sm-7 col-lg-7  text-left">
                <h3> AGENTS DE DÉMOULAGE</h3>
                <p>Conçus pour plusieurs applications telles que la biscuiterie, la boulangerie pâtisserie, la
                    confiserie… Ces compounds (ou agents démoulants) sont à base de mélanges à base d’huiles végétales
                    (soja, colza, etc.), de cires végétales et d’émulsifiants (lécithine de soja, lécithine de colza,
                    mono- et diglycérides) et éventuellement des antioxydants.
                </p>
                <p>Ces mélanges doivent bien évidemment répondre à des caractéristiques techniques telles que la bonne
                    aptitude à la lubrification, la bonne aptitude à la pulvérisation, l’efficacité sur un grand
                    intervalle de température, une bonne stabilité physique afin d’éviter les phénomènes de déphasage,
                    de décantation. Ils doivent également offrir un certain nombre de caractéristiques chimiques : une
                    bonne stabilité à l’oxydation, un point de fumée élevé et une faible tendance à la polymérisation
                    afin d’éviter l’encrassement des moules.</p>
                <p>Enfin, sur le plan organoleptique, les démoulants doivent avoir un goût et une odeur neutres afin de
                    ne pas perturber la qualité du produit de base.
                </p>
            </div>

            <div class="col-sm-5 col-lg-5  text-left">
                <div class="owl-carousel owl-theme" id="product_carousel">
                    <div class="item">
                        <article class="vertical-item content-padding post format-standard with_background rounded">
                            <div class="item-media">
                                <img src="files/img/AGENTS_DEMOULAGE/1P.jpg" alt="">
                                <div class="media-links">
                                    <div class="links-wrap">
                                        <a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]"
                                            href="files/img/AGENTS_DEMOULAGE/1P.jpg"></a>
                                    </div>
                                </div>
                            </div>

                        </article>
                    </div>
                    <div class="item">
                        <article class="vertical-item content-padding post format-standard with_background rounded">
                            <div class="item-media">
                                <img src="files/img/AGENTS_DEMOULAGE/2P.jpg" alt="">
                                <div class="media-links">
                                    <div class="links-wrap">
                                        <a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]"
                                            href="files/img/AGENTS_DEMOULAGE/2P.jpg"></a>
                                    </div>
                                </div>
                            </div>

                        </article>
                    </div>
                    <div class="item">
                        <article class="vertical-item content-padding post format-standard with_background rounded">
                            <div class="item-media">
                                <img src="files/img/AGENTS_DEMOULAGE/3P.jpg" alt="">
                                <div class="media-links">
                                    <div class="links-wrap">
                                        <a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]"
                                            href="files/img/AGENTS_DEMOULAGE/3P.jpg"></a>
                                    </div>
                                </div>
                            </div>

                        </article>
                    </div>
                    <div class="item">
                        <article class="vertical-item content-padding post format-standard with_background rounded">
                            <div class="item-media">
                                <img src="files/img/AGENTS_DEMOULAGE/4P.jpg" alt="">
                                <div class="media-links">
                                    <div class="links-wrap">
                                        <a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]"
                                            href="files/img/AGENTS_DEMOULAGE/4P.jpg"></a>
                                    </div>
                                </div>
                            </div>

                        </article>
                    </div>
                    <div class="item">
                        <article class="vertical-item content-padding post format-standard with_background rounded">
                            <div class="item-media">
                                <img src="files/img/AGENTS_DEMOULAGE/5P.png" alt="">
                                <div class="media-links">
                                    <div class="links-wrap">
                                        <a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]"
                                            href="files/img/AGENTS_DEMOULAGE/5P.jpg"></a>
                                    </div>
                                </div>
                            </div>

                        </article>
                    </div>

                </div>

            </div>

        </div>
        <div class="row" style="margin-bottom:50px;">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <p>
                    <strong>
                    INGREDIS TUNISIE formule et commercialise plusieurs types d'agents de démoulage selon application et type de produit à démouler.


                    </strong>
                    <p>
                    Domaines d’application :
                    </p>
                <ul>
                    <li>Moules et plaques en Biscuiterie</li>
                    <li>Boulangerie</li>
                    <li>Confiserie</li>
                    <li>...</li>

                </ul>
               
                </p>
            </div>
            
        </div>
    </div>
</section>
</div>




<script>
$("#product_carousel").owlCarousel({

    navigation: true, // Show next and prev buttons

    slideSpeed: 300,
    paginationSpeed: 400,

    items: 1,
    itemsDesktop: false,
    itemsDesktopSmall: false,
    itemsTablet: false,
    itemsMobile: false

});
</script>

<?php
include ("footer.php");
?>