			
			

<footer class="page_footer ds section_padding_top_50 section_padding_bottom_65 columns_padding_25">
	<div class="container">
		<div class="row">

			<div class="col-md-2 col-sm-12 text-center">
				<div class="widget"  style="padding-top: 25px;">
					<!--<a href="./" class="logo bg_logo bottommargin_25">-->
					<img src="files/ing.png" alt="">
					<!--</a>-->
									</div>
			</div>

			<div class="col-md-10 col-sm-12 text-left">

					<div class="line-height-thin">

						<div class="media small-teaser inline-block margin_0">
						
						 <div class="media-left media-middle">
							<i class="rt-icon2-location2 highlight2" aria-hidden="true"></i>
							</div>
							<div class="media-body media-middle grey">
								Adresse : INGREDIS TUNISIE SARL 55, RUE DE LA PAIX, 8030 GROMBALIA, TUNISIE
							</div>
						</div>
						<br>
						<div class="media small-teaser inline-block margin_0">
							<div class="media-left media-middle">
								<i class="rt-icon2-phone4 highlight2" aria-hidden="true"></i>
							</div>
							<div class="media-body media-middle grey">
							    TEL : (+216) 36 040 191 / 72 399 262 
								
							</div>
						</div>
						
									<br>
						<div class="media small-teaser inline-block margin_0">
							<div class="media-left media-middle">
							
								<i class="rt-icon2-files highlight2"></i>
							</div>
							<div class="media-body media-middle grey"> 
								FAX : (+216) 72 399 262
								
							</div>
						</div>
						
						<br>
						<div class="media small-teaser inline-block margin_0">
							<div class="media-left media-middle">
						
									<i class="rt-icon2-store highlight2"></i>
							</div>
							<div class="media-body media-middle grey">
								MAGASIN : KYLOG LOGISTICS, AV MOHAMED ALI, ZI RADES, BEN AROUS
							</div>
						</div>
						<br>
	            	</div>
	         </div>
			 
	 
			 
</div>
</div>
</footer>
			<section class="ls page_copyright section_padding_15">
				<div class="container">
					<div class="row">
						<div class="col-sm-12 text-center">
							<p class="small-text small-spacing grey">
														
Copyright © 2021 INGREDIS TUNISIE  . Tous droits réservés . Création par <br>  <a target="_blank" href="https://proxiweb.tn"><img src="https://www.jinfo.fr/images/proxiweb.jpg" width="150" height="30"></a>
								
							</p>
						</div>
					</div>
				</div>
			</section>

		</div>
		<!-- eof #box_wrapper -->
	</div>
	<!-- eof #canvas -->

	<script src="files/compressed.js"></script>
	<script src="files/main.js"></script>


 



<a href="//?lang=en#" id="toTop" style="display: none;"><span id="toTopHover"></span>To Top</a></body></html>