<?php
include ("header.php");
?>

<section id="about" class="ls section_padding_top_25">

    <div class="container">
        <div class="row">

            <div class="col-sm-7 col-lg-7  text-left">
                <h3>additives and ingredients</h3>
                <p>The vocation of INGREDIS TUNISIA has always been to act not only as a supplier of raw materials, but also as a real partner of our customers, offering concrete solutions from the technical point of view in the field of the agri-food industries:  
                </p>
                <ul>
                    <li>Collaboration in R&D projects with companies and technical centers. </li>
                    <li>Development and manufacture of custom formulations.</li>
                    <li>Technical advice on additives and legislation.</li>
                    <li>Technical advice on additives and legislation.</li>
                </ul>
            </div>

            <div class="col-sm-5 col-lg-5  text-left">
                <div class="owl-carousel owl-theme" id="product_carousel">
                    <div class="item">
                        <article class="vertical-item content-padding post format-standard with_background rounded">
                            <div class="item-media">
                                <img src="files/img/ADDITIFS_INGREDIENTS/1P.jpeg" alt="">
                                <div class="media-links">
                                    <div class="links-wrap">
                                        <a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]"
                                            href="files/img/ADDITIFS_INGREDIENTS/1P.jpeg"></a>
                                    </div>
                                </div>
                            </div>

                        </article>
                    </div>
                    <div class="item">
                        <article class="vertical-item content-padding post format-standard with_background rounded">
                            <div class="item-media">
                                <img src="files/img/ADDITIFS_INGREDIENTS/2P.jpg" alt="">
                                <div class="media-links">
                                    <div class="links-wrap">
                                        <a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]"
                                            href="files/img/ADDITIFS_INGREDIENTS/2P.jpg"></a>
                                    </div>
                                </div>
                            </div>

                        </article>
                    </div>
                    <div class="item">
                        <article class="vertical-item content-padding post format-standard with_background rounded">
                            <div class="item-media">
                                <img src="files/img/ADDITIFS_INGREDIENTS/3P.webp" alt="">
                                <div class="media-links">
                                    <div class="links-wrap">
                                        <a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]"
                                            href="files/img/ADDITIFS_INGREDIENTS/3P.webp"></a>
                                    </div>
                                </div>
                            </div>

                        </article>
                    </div>
                    <div class="item">
                        <article class="vertical-item content-padding post format-standard with_background rounded">
                            <div class="item-media">
                                <img src="files/img/ADDITIFS_INGREDIENTS/4P.png" alt="">
                                <div class="media-links">
                                    <div class="links-wrap">
                                        <a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]"
                                            href="files/img/ADDITIFS_INGREDIENTS/4P.png"></a>
                                    </div>
                                </div>
                            </div>

                        </article>
                    </div>
                    <div class="item">
                        <article class="vertical-item content-padding post format-standard with_background rounded">
                            <div class="item-media">
                                <img src="files/img/ADDITIFS_INGREDIENTS/5P.jpg" alt="">
                                <div class="media-links">
                                    <div class="links-wrap">
                                        <a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]"
                                            href="files/img/ADDITIFS_INGREDIENTS/5P.jpg"></a>
                                    </div>
                                </div>
                            </div>

                        </article>
                    </div>

                </div>

            </div>

        </div>
        <div class="row">
            <div class="col-12">
                <p>We offer a range of additives and ingredients for the formulation of agri-food products:</p>
            </div>
        </div>
        <div class="row" style="margin-bottom:50px;">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <p>
                    <strong>
                    ANTI-OXIDANT 
                    </strong>
                <ul>
                    <li>EDTA (E385) (Ca, Na)</li>
                    <li>EDTA (E385) LIQUID</li>
                    <li>BHA (E320)</li>
                    <li>BHT (E321)</li>
                    <li>Propyl gallate (E310)</li>
                    <li>TBHQ (E319)</li>
                    <li>...</li>
                </ul>
                </p>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">

                <strong>
                ADDITIVES & OTHER INGREDIENTS :
                </strong>
                <ul>
                    <li>
                    Ascorbic acid (E300)
                    </li>
                    <li>citric acid (E330)</li>
                    <li>lactic Acid (E270)</li>
                    <li>Phosphoric Acid (E338) </li>
                    <li>Modified Starch (E1422)</li>
                    <li>Sodium Benzoate (E211)</li>
                    <li>chloride calcium (E509)</li>
                    <li>sodium citrate (E331)</li>
                    <li>CMC Carboxymethylcellulose (E466)</li>
                    <li>Yeast extract (flavour enhancer)</li>
                    <li>malt extract</li>
                    <li>Fibre (bamboo, sugar cane, wheat...)</li>
                    <li>guar gum (E412)</li>
                    <li>Xanthan Gum (E415)</li>
                    <li>Dried Vegetables in Powder</li>
                    <li>Maltodextrin</li>
                    <li>Sodium metabisulphite (E223)</li>
                    <li>Soy Protein</li>
                    <li>Pyrophosphates (SAPP 28)</li>
                    <li>Potassium Sorbate (E202)</li>
                    <li> Tri Polyphosphates (STPP)</li>
                    <li>...</li>
                </ul>
            </div>
        </div>
    </div>
</section>
</div>




<script>
$("#product_carousel").owlCarousel({

    navigation: true, // Show next and prev buttons

    slideSpeed: 300,
    paginationSpeed: 400,

    items: 1,
    itemsDesktop: false,
    itemsDesktopSmall: false,
    itemsTablet: false,
    itemsMobile: false

});
</script>

<?php
include ("footer.php");
?>