<?php
include ("header.php");
?>

<section id="about" class="ls section_padding_top_25">

    <div class="container">
        <div class="row">

            <div class="col-sm-7 col-lg-7  text-left">
                <h3> mold release agents</h3>
                <p>Designed for several applications such as biscuit, bakery, confectionery, etc. These compounds (or demoulants)
                     are based on mixtures based on vegetable oils (soya, rapeseed, etc.),
                      vegetable waxes and emulsifiers (soya lecithin, rapeseed lecithin, mono- and diglycerides) 
                      and possibly antioxidants.
                </p>
                <p>These mixtures must obviously meet technical specifications such as the correct
                    ability to lubricate, good ability to spray, efficiency on a large
                    temperature range, good physical stability in order to avoid phase shift phenomena,
                    decantation. They must also offer a number of chemical characteristics: a
                    good oxidation stability, a high smoke point and a low tendency to polymerization
                    to avoid fouling the moulds.</p>
                <p>Finally, organoleptically, demoulants must have a neutral taste and smell in order to
                    do not disturb the quality of the basic product.
                </p>
            </div>

            <div class="col-sm-5 col-lg-5  text-left">
                <div class="owl-carousel owl-theme" id="product_carousel">
                    <div class="item">
                        <article class="vertical-item content-padding post format-standard with_background rounded">
                            <div class="item-media">
                                <img src="files/img/AGENTS_DEMOULAGE/1P.jpg" alt="">
                                <div class="media-links">
                                    <div class="links-wrap">
                                        <a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]"
                                            href="files/img/AGENTS_DEMOULAGE/1P.jpg"></a>
                                    </div>
                                </div>
                            </div>

                        </article>
                    </div>
                    <div class="item">
                        <article class="vertical-item content-padding post format-standard with_background rounded">
                            <div class="item-media">
                                <img src="files/img/AGENTS_DEMOULAGE/2P.jpg" alt="">
                                <div class="media-links">
                                    <div class="links-wrap">
                                        <a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]"
                                            href="files/img/AGENTS_DEMOULAGE/2P.jpg"></a>
                                    </div>
                                </div>
                            </div>

                        </article>
                    </div>
                    <div class="item">
                        <article class="vertical-item content-padding post format-standard with_background rounded">
                            <div class="item-media">
                                <img src="files/img/AGENTS_DEMOULAGE/3P.jpg" alt="">
                                <div class="media-links">
                                    <div class="links-wrap">
                                        <a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]"
                                            href="files/img/AGENTS_DEMOULAGE/3P.jpg"></a>
                                    </div>
                                </div>
                            </div>

                        </article>
                    </div>
                    <div class="item">
                        <article class="vertical-item content-padding post format-standard with_background rounded">
                            <div class="item-media">
                                <img src="files/img/AGENTS_DEMOULAGE/4P.jpg" alt="">
                                <div class="media-links">
                                    <div class="links-wrap">
                                        <a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]"
                                            href="files/img/AGENTS_DEMOULAGE/4P.jpg"></a>
                                    </div>
                                </div>
                            </div>

                        </article>
                    </div>
                    <div class="item">
                        <article class="vertical-item content-padding post format-standard with_background rounded">
                            <div class="item-media">
                                <img src="files/img/AGENTS_DEMOULAGE/5P.png" alt="">
                                <div class="media-links">
                                    <div class="links-wrap">
                                        <a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]"
                                            href="files/img/AGENTS_DEMOULAGE/5P.jpg"></a>
                                    </div>
                                </div>
                            </div>

                        </article>
                    </div>

                </div>

            </div>

        </div>
        <div class="row" style="margin-bottom:50px;">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <p>
                    <strong>
                    INGREDIS TUNISIE formulates and markets several types of release agents depending on the application and type of product to be removed.
                    </strong>
                    <p>
                    areas of application :
                    </p>
                <ul>
                    <li>Biscuit moulds and plates</li>
                    <li>Bakery</li>
                    <li>Confectionery</li>
                    <li>...</li>

                </ul>
               
                </p>
            </div>
            
        </div>
    </div>
</section>
</div>




<script>
$("#product_carousel").owlCarousel({

    navigation: true, // Show next and prev buttons

    slideSpeed: 300,
    paginationSpeed: 400,

    items: 1,
    itemsDesktop: false,
    itemsDesktopSmall: false,
    itemsTablet: false,
    itemsMobile: false

});
</script>

<?php
include ("footer.php");
?>