<?php
include ("header.php");
?>	
	<section class="ls section_padding_top_25 section_padding_bottom_150">
		<div class="container">
			<div class="row columns_padding_25 columns_margin_bottom_20">
				<div class="col-md-4">
					<img src="files/panaderia.jpg" width="528" alt="">
				</div>
				<div class="col-md-8">
					<h2>
					Welcome to Ingredis Tunisia
				</h2>
					<p>
					Ingredis Tunisia was created in October 1997 by professionals with extensive experience in the food sector. It began operations in 1998 as a distributor and agent for the sale of ingredients and additives for the food industry.	</p>
					<p>
					The objective of the company is to provide a dynamic and efficient technical and commercial service to ensure the satisfaction and trust of our customers. To do this, we have a team of qualified professionals with extensive industry experience and specific training to provide a customized solution for each case.			</p>
					<p>
					From the very beginning, we have worked with a diverse group of industry-leading suppliers with R&amp;D departments developing and providing the food industry with a range of new and innovative solutions to the challenges they face. Today we have suppliers in Europe, America and Asia				</p>
				</div>
			</div>
<!--
			<div class="row">
				
				<h2>
					Pourquoi Ingredis Tunisie?				</h2>

				<div class="col-sm-6">
					<div class="panel-group" id="accordion1">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a data-toggle="collapse" data-parent="#accordion1" href="societe.php#collapse1">
										Grâce à notre équipe								</a>
								</h4>
							</div>
							<div id="collapse1" class="panel-collapse collapse in">
								<div class="panel-body">
									Notre équipe est formée de <strong>professionnels avec une vaste expérience</strong> dans les différents secteurs de l'industrie alimentaire et avec les connaissances nécessaires pour trouver la meilleure solution technique pour chaque application. En raison de la nature exigeante de notre marché, cette équipe est <strong>entièrement formée de diplômés universitaires</strong> spécialisés dans les différentes branches de l'industrie. De cette façon, nous pouvons garantir un dialogue approprié avec nos clients et clients.			</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a data-toggle="collapse" data-parent="#accordion1" href="societe.php#collapse2" class="collapsed">
										En raison de la qualité de nos produits							</a>
								</h4>
							</div>
							<div id="collapse2" class="panel-collapse collapse">
								<div class="panel-body">
							Une gamme de produits fabriqués par des entreprises internationales dont les <strong>normes technologiques élevées et la capacité de production sont la meilleure garantie possible</strong>. Tous ces produits bénéficient d'une <strong>certification de qualité et de traçabilité</strong> via les documents correspondants et apportent à nos clients des <strong>solutions techniques et économiques</strong> pour tout processus industriel. 			</div>
							</div>
						</div>
					</div>
				</div>

				<div class="col-sm-6">
					<div class="panel-group" id="accordion2">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a data-toggle="collapse" data-parent="#accordion2" href="societe.php#collapse3" class="" aria-expanded="true">
										Grâce à une structure efficace								</a>
								</h4>
							</div>
							<div id="collapse3" class="panel-collapse collapse in" aria-expanded="true" style="">
								<div class="panel-body">
						Une <strong>organisation administrative rapide et fiable</strong>, toujours prête à résoudre les problèmes des clients de la meilleure façon et dans les plus brefs délais.							</div>
							</div>
						</div>

						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a data-toggle="collapse" data-parent="#accordion2" href="societe.php#collapse4" class="collapsed" aria-expanded="false">
										Grâce à un service logistique fiable								</a>
								</h4>
							</div>
							<div id="collapse4" class="panel-collapse collapse" aria-expanded="false">
								<div class="panel-body">
									Un système logistique qui comprend <strong>le stockage, la livraison et le transport</strong> et qui offre une <strong>solution fiable et efficace</strong> selon nos besoins et ceux de nos clients.							</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>-->
	</section>

<?php
include ("footer.php");
?>	