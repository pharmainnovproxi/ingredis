<!DOCTYPE html>
<!-- saved from url=(0036)//?lang=en -->
<html class=" js flexbox canvas canvastext webgl no-touch geolocation postmessage websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers no-applicationcache svg inlinesvg smil svgclippaths" style=""><!--<![endif]--><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Ingrédis Tunisia</title>
	
	<!--[if IE]>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<![endif]-->
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

	<link rel="stylesheet" href="files/bootstrap.min.css">
	<link rel="stylesheet" href="files/animations.css">
	<link rel="stylesheet" href="files/fonts.css">
	<link rel="stylesheet" href="files/main.css" class="color-switcher-link">
	<link rel="stylesheet" href="files/custom.css">
	
	<script src="files/modernizr-2.6.2.min.js"></script>

	<!--[if lt IE 9]>
		<script src="/js/vendor/html5shiv.min.js"></script>
		<script src="/js/vendor/respond.min.js"></script>
		<script src="/js/vendor/jquery-1.12.4.min.js"></script>
	<![endif]-->

</head>

<body>
	<!--[if lt IE 9]>
		<div class="bg-danger text-center">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/" class="highlight">upgrade your browser</a> to improve your experience.</div>
	<![endif]-->



	

	<!-- search modal -->
	<div class="modal" tabindex="-1" role="dialog" aria-labelledby="search_modal" id="search_modal">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">
				<i class="rt-icon2-cross2"></i>
			</span>
		</button>
		<div class="widget widget_search">
			<form method="get" class="searchform search-form form-inline" action="//">
				<div class="form-group">
					<input type="text" value="" name="search" class="form-control" placeholder="Search keyword" id="modal-search-input">
				</div>
				<button type="submit" class="theme_button">Research</button>
			</form>
		</div>
	</div>

	<!-- Unyson messages modal -->
	<div class="modal fade" tabindex="-1" role="dialog" id="messages_modal">
		<div class="fw-messages-wrap ls with_padding">
			<!-- Uncomment this UL with LI to show messages in modal popup to your user: -->
			<!--
		<ul class="list-unstyled">
			<li>Message To User</li>
		</ul>
		-->

		</div>
	</div>
	<!-- eof .modal -->

	<!-- wrappers for visual page editor and boxed version of template -->
	<div id="canvas">
		<div id="box_wrapper">

			<!-- template sections -->

			<section class="page_topline cs two_color section_padding_top_5 section_padding_bottom_5 table_section">
				<div class="container-fluid">
					<div class="row">
						<div class="col-sm-6 text-center text-sm-left">
							<p class="divided-content">
								<span class="medium black">
									Call: (+216) 72399262 / (+216) 36040191  								</span>
								
								<!--<a href="#">How to Find Us</a>
								<a href="#">Give Feedback</a>-->
							</p>
						</div>
						<div class="col-sm-6 text-center text-sm-right">

							<div class="widget widget_search">
							
								<span class="medium black">
									 
									<a href="index.php">English</a> &nbsp; | &nbsp; 
									<a href="../index.php">Français</a>
								</span>
								<!--
								<form method="get" class="searchform form-inline" action="./">
									<div class="form-group-wrap">
										<div class="form-group margin_0">
											<label class="sr-only" for="topline-search">Search for:</label>
											<input id="topline-search" type="text" value="" name="search" class="form-control" placeholder="Search Keyword">
										</div>
										<button type="submit" class="theme_button">Search</button>
									</div>
								</form>
								-->
							</div>

						</div>
					</div>
				</div>
			</section>

			<div class="page_header_wrapper header_white affix-top-wrapper" style="height: 120px;"><header class="page_header header_white toggler_xs_right section_padding_20 affix-top">
				<div class="container-fluid">
					<div class="row">
						<div class="col-sm-12 display_table">
							<div class="header_left_logo display_table_cell">
								<a href="index.php" class="logo top_logo">
									<img src="files/ing.png" alt="">
									<!--
									<span class="logo_text">
										<span class="big">GoGreen</span>
										<span class="small-text">Recycling</span>
									</span>
									-->
								</a>
							</div>

							<div class="header_mainmenu display_table_cell text-center">
								<!-- main nav start -->
								<nav class="mainmenu_wrapper" style="">
									<ul class="mainmenu nav sf-menu sf-js-enabled sf-arrows" style="touch-action: pan-y;">

										<li>
											<a href="index.php">Home</a>
																					</li>

										<li>
											<a href="societe.php">Society</a>
																					</li>
										<!-- eof pages 

										<li>
											<a href="secteurs.php">Secteurs</a>
																					</li>
										 eof features 


										<li>
											<a href="produits.php">Produits</a>
																					</li>-->
										<li>
  <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
  Produits
  </a>

  <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
    <a class="dropdown-item" href="enzymes.php">ENZYMES</a>
    <a class="dropdown-item" href="emulsifiants.php">EMULSIFIERS</a>
    <a class="dropdown-item" href="lecithines.php">LECITHINS</a>
    <a class="dropdown-item" href="agents_demoulage.php">ADDITIVES, INGREDIENTS,</a>
    <a class="dropdown-item" href="additifs_ingredients.php">CHEMICAL PRODUCTS</a>

  </div>
  </li>

										<li>
											<a href="contact.php">Contact</a>
																					</li>
										<!-- eof blog -->

																			</ul>
								</nav>
								<!-- eof main nav -->

								<!-- header toggler -->
								<span class="toggle_menu">
									<span></span>
								</span>
							</div>

													</div>
					</div>
				</div>
			</header></div>