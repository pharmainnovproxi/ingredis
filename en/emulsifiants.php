<?php
include ("header.php");
?>

<section id="about" class="ls section_padding_top_25">

    <div class="container">
        <div class="row">

            <div class="col-sm-7 col-lg-7  text-left">
                <h3>SYNTHETIC EMULSIFIERS (ESTERS)</h3>
                <p>Emulsifiers are a key ingredient in the manufacture of baking improvers
                    quality, but they are also used in many other areas of industry
                    food. Emulsifiers affect appearance, texture and structure, while helping to
                    maintain quality and freshness.
                </p>
                <p>Our emulsifiers use high-quality vegetable oil, palm oil or decolza oil as the main raw materials.
                </p>
                <p>From mono- and di-glycerides to more sophisticated esters, our exclusive partner has developed
                    a range of fatty acid derivatives: pure, co-sprayed for many sectors of industry
                    food. In addition to baking emulsifiers, we supply many other
                    segments such as emulsifiers for making "cake gel improver", products
                    dairy, margarine, sauces, potato flakes, cold cuts...</p>
            </div>

            <div class="col-sm-5 col-lg-5  text-left">
                <div class="owl-carousel owl-theme" id="product_carousel">
                    <div class="item">
                        <article class="vertical-item content-padding post format-standard with_background rounded">
                            <div class="item-media">
                                <img src="files/img/ESTERS/1P.jpg" alt="">
                                <div class="media-links">
                                    <div class="links-wrap">
                                        <a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]"
                                            href="files/img/ESTERS/1P.jpg"></a>
                                    </div>
                                </div>
                            </div>

                        </article>
                    </div>
                    <div class="item">
                        <article class="vertical-item content-padding post format-standard with_background rounded">
                            <div class="item-media">
                                <img src="files/img/ESTERS/2P.jpg" alt="">
                                <div class="media-links">
                                    <div class="links-wrap">
                                        <a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]"
                                            href="files/img/ESTERS/2P.jpg"></a>
                                    </div>
                                </div>
                            </div>

                        </article>
                    </div>
                    <div class="item">
                        <article class="vertical-item content-padding post format-standard with_background rounded">
                            <div class="item-media">
                                <img src="files/img/ESTERS/3P.jpg" alt="">
                                <div class="media-links">
                                    <div class="links-wrap">
                                        <a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]"
                                            href="files/img/ESTERS/3P.jpg"></a>
                                    </div>
                                </div>
                            </div>

                        </article>
                    </div>
                    <div class="item">
                        <article class="vertical-item content-padding post format-standard with_background rounded">
                            <div class="item-media">
                                <img src="files/img/ESTERS/4P.png" alt="">
                                <div class="media-links">
                                    <div class="links-wrap">
                                        <a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]"
                                            href="files/img/ESTERS/4P.png"></a>
                                    </div>
                                </div>
                            </div>

                        </article>
                    </div>
                    <div class="item">
                        <article class="vertical-item content-padding post format-standard with_background rounded">
                            <div class="item-media">
                                <img src="files/img/ESTERS/5P.jpg" alt="">
                                <div class="media-links">
                                    <div class="links-wrap">
                                        <a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]"
                                            href="files/img/ESTERS/5P.jpg"></a>
                                    </div>
                                </div>
                            </div>

                        </article>
                    </div>

                </div>

            </div>

        </div>
        <div class="row" style="margin-bottom:50px;">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <p>
                    <strong>
                    The INGREDIS TUNISIE emulsifier portfolio includes:
                    </strong>
                <ul>
                    <li>E471 40%, 60% monoglycerides and 90% distilled</li>
                    <li>E475 (Polyglycerol esters)</li>
                    <li>E476 (PGPR)</li>
                    <li>E472a, E472b, E472c, E472e</li>
                    <li>E491, E492 (Sorbitan stearate) </li>
                    <li>Oil binders & Crystal promoters</li>
                    <li> activated emulsifier systems</li>
                    <li>...</li>

                </ul>
                </p>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">

                <strong>
                INGREDIS TUNISIE is a reference as a distributor of emulsifiers offering a wide range for different applications.:
                </strong>
                <ul>
                    <li>
                    Bakery
                    </li>
                    <li> Cakes and Cookies</li>
                    <li>Chocolate and confectionery</li>
                    <li>Ice cream and dairy products</li>
                    <li>Margarines and spreads</li>
                    <li>Deli Meats</li>
                    <li>Milling</li>
                    <li>Production of chips</li>
                    <li>Snacks and Seasonings</li>
                    <li>Animal feed.</li>

                </ul>
            </div>
        </div>
    </div>
</section>
</div>




<script>
$("#product_carousel").owlCarousel({

    navigation: true, // Show next and prev buttons

    slideSpeed: 300,
    paginationSpeed: 400,

    items: 1,
    itemsDesktop: false,
    itemsDesktopSmall: false,
    itemsTablet: false,
    itemsMobile: false

});
</script>

<?php
include ("footer.php");
?>