<?php
include ("header.php");
?>

<section id="about" class="ls section_padding_top_25">

    <div class="container">
        <div class="row">

            <div class="col-sm-7 col-lg-7  text-left">
                <h3> LECITHINS (E322)</h3>
                <p>As an emulsifier, soy lecithin is used in food applications as an aeration agent, viscosity regulator, dispersant and lubricant.
Typically, an emulsion is a suspension of small droplets of a liquid in another liquid with which it is unable to mix: Oil in water (O/W) and water in oil (W/O) are the two main types of emulsions.
                </p>
                <p>The molecular structure of lecithin gives it the characteristics of an effective emulsifier for the interaction of water and oil. Phospholipids, the main component of lecithin, are partly hydrophilic (attracted by water) and partly hydrophobic (repelled by water). It is the ability of lecithin to interact simultaneously with oil and water that makes it an effective and stable emulsifier.</p>
                <p>When introduced into a system, an emulsifier such as lecithin helps to maintain a stable emulsion between two non-mible liquids. The emulsifier reduces the surface tension between the two liquids and allows them to mix and form a stable heterogeneous dispersion.</p>
            </div>

            <div class="col-sm-5 col-lg-5  text-left">
                <div class="owl-carousel owl-theme" id="product_carousel">
                    <div class="item">
                        <article class="vertical-item content-padding post format-standard with_background rounded">
                            <div class="item-media">
                                <img src="files/img/LECITHINES/1P.jpg" alt="">
                                <div class="media-links">
                                    <div class="links-wrap">
                                        <a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]"
                                            href="files/img/LECITHINES/1P.jpg"></a>
                                    </div>
                                </div>
                            </div>

                        </article>
                    </div>
                    <div class="item">
                        <article class="vertical-item content-padding post format-standard with_background rounded">
                            <div class="item-media">
                                <img src="files/img/LECITHINES/2P.jpg" alt="">
                                <div class="media-links">
                                    <div class="links-wrap">
                                        <a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]"
                                            href="files/img/LECITHINES/2P.jpg"></a>
                                    </div>
                                </div>
                            </div>

                        </article>
                    </div>
                    <div class="item">
                        <article class="vertical-item content-padding post format-standard with_background rounded">
                            <div class="item-media">
                                <img src="files/img/LECITHINES/3P.jpg" alt="">
                                <div class="media-links">
                                    <div class="links-wrap">
                                        <a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]"
                                            href="files/img/LECITHINES/3P.jpg"></a>
                                    </div>
                                </div>
                            </div>

                        </article>
                    </div>
                    <div class="item">
                        <article class="vertical-item content-padding post format-standard with_background rounded">
                            <div class="item-media">
                                <img src="files/img/LECITHINES/4P.jpg" alt="">
                                <div class="media-links">
                                    <div class="links-wrap">
                                        <a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]"
                                            href="files/img/LECITHINES/4P.jpg"></a>
                                    </div>
                                </div>
                            </div>

                        </article>
                    </div>
                    <div class="item">
                        <article class="vertical-item content-padding post format-standard with_background rounded">
                            <div class="item-media">
                                <img src="files/img/LECITHINES/5P.jpg" alt="">
                                <div class="media-links">
                                    <div class="links-wrap">
                                        <a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]"
                                            href="files/img/LECITHINES/5P.jpg"></a>
                                    </div>
                                </div>
                            </div>

                        </article>
                    </div>
                    
                </div>

            </div>

        </div>
        <div class="row" style="margin-bottom:50px;">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <p>
                    <strong>
                    Main uses: :


                    </strong>
                <ul>
                    <li>Chocolate And Confectionery</li>
                    <li>Margarine</li>
                    <li>Bakery</li>
                    <li>Instant Drinks</li>
                    <li>Paint formulation (non food)</li>
                    <li>...</li>

                </ul>
                <strong>
                In practice, typical assays of lecithin in an emulsion vary as follows:




                    </strong>
                <ul>
                    <li>1-5% MG for W/O system</li>
                    <li>5-10% MG for O/W</li>
                

                </ul>
                <p>The amount of lecithin used depends on factors such as HLB,  pH, protein and gum inclusion, and salt concentration.

</p>
                </p>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                
                <strong>
                The main references marketed by INGREDIS TUNISIE are:
                </strong>
                <ul>
                    <li>Soya lecithin (VEROLEC).</li>
                    <li>Lécithine de soja NON OGM IP.</li>
                    <li>Sunflower lecithin (GIRALEC).</li>
                    <li>Lecithin of rapeseed.</li>
                    <li>Lecithin flour for bakery (LECISOL).</li>
                    <li>Lecithin for instant drink powder (chocolate powder).</li>
                    <li>Powdered lecithin</li>
                    <li>...</li>

                </ul>
            </div>
        </div>
    </div>
</section>
</div>




<script>
$("#product_carousel").owlCarousel({

    navigation: true, // Show next and prev buttons

    slideSpeed: 300,
    paginationSpeed: 400,

    items: 1,
    itemsDesktop: false,
    itemsDesktopSmall: false,
    itemsTablet: false,
    itemsMobile: false

});
</script>

<?php
include ("footer.php");
?>