<?php
include ("header.php");
?>

<section id="about" class="ls section_padding_top_25">

    <div class="container">
        <div class="row">

            <div class="col-sm-7 col-lg-7  text-left">
                <h3>ENZYMES</h3>
                <p>Enzymes are proteins that act as catalysts for an unlimited range of
                    biological processes. They are highly specific processing aids and constitute
                    very effective tools for breaking down complex molecules into simple elemental components
                    or to help create or stabilize large molecular structures. Their presence in all
                    living organisms demonstrate their essential role in the life process.
                </p>
                <p>In addition, modern biotechnology has enabled the development of enzymes produced by
                    fermentation, using the latest strain improvement techniques to select
                    microorganisms of fungal or bacterial origin capable of producing pure and safe enzymes.
                    Our German partner is at the forefront of this science and has developed a range of enzymes
                    multipurpose food and food.</p>
            </div>

            <div class="col-sm-5 col-lg-5  text-left">
                <div class="owl-carousel owl-theme" id="product_carousel">
                    <div class="item">
                        <article class="vertical-item content-padding post format-standard with_background rounded">
                            <div class="item-media">
                                <img src="files/img/ENZYMES/1P.jpg" alt="">
                                <div class="media-links">
                                    <div class="links-wrap">
                                        <a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]"
                                            href="files/img/ENZYMES/1P.jpg"></a>
                                    </div>
                                </div>
                            </div>

                        </article>
                    </div>
                    <div class="item">
                        <article class="vertical-item content-padding post format-standard with_background rounded">
                            <div class="item-media">
                                <img src="files/img/ENZYMES/2P.jpg" alt="">
                                <div class="media-links">
                                    <div class="links-wrap">
                                        <a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]"
                                            href="files/img/ENZYMES/2P.jpg"></a>
                                    </div>
                                </div>
                            </div>

                        </article>
                    </div>
                    <div class="item">
                        <article class="vertical-item content-padding post format-standard with_background rounded">
                            <div class="item-media">
                                <img src="files/img/ENZYMES/3P.jpg" alt="">
                                <div class="media-links">
                                    <div class="links-wrap">
                                        <a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]"
                                            href="files/img/ENZYMES/3P.jpg"></a>
                                    </div>
                                </div>
                            </div>

                        </article>
                    </div>
                    <div class="item">
                        <article class="vertical-item content-padding post format-standard with_background rounded">
                            <div class="item-media">
                                <img src="files/img/ENZYMES/4P.jpg" alt="">
                                <div class="media-links">
                                    <div class="links-wrap">
                                        <a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]"
                                            href="files/img/ENZYMES/4P.jpg"></a>
                                    </div>
                                </div>
                            </div>

                        </article>
                    </div>
                    <div class="item">
                        <article class="vertical-item content-padding post format-standard with_background rounded">
                            <div class="item-media">
                                <img src="files/img/ENZYMES/5P.webp" alt="">
                                <div class="media-links">
                                    <div class="links-wrap">
                                        <a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]"
                                            href="files/img/ENZYMES/5P.webp"></a>
                                    </div>
                                </div>
                            </div>

                        </article>
                    </div>
                    
                </div>

            </div>

        </div>
        <div class="row" style="margin-bottom:50px;">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <p>
                    <strong>
                    The main categories of enzymes that we market in exclusive partnership with our
                      German partner, are presented below:
                    </strong>
                <ul>
                    <li>Xylanases</li>
                    <li>Alpha amylases</li>
                    <li>Glucose oxydases</li>
                    <li>Lipases</li>
                    <li>protease</li>
                    <li>Pectinase</li>
                    <li>...</li>

                </ul>
                </p>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <p>
                Enzymes must be accurately measured, an overdose leading to a defect in the
                    product. In most cases, enzymes are hydrolases that depolymerize:
                    proteins, carbohydrate polymers, plant wall polymers and lipids
                    triglycérique.
                </p>
                <strong>
                Our enzymes are used in the following applications:
                </strong>
                <ul>
                    <li>Bakery: bread improvers</li>
                    <li>cakes and biscuits</li>
                    <li>Milling: preparation of improved flours</li>
                    <li>animal feed</li>


                </ul>
            </div>
        </div>
    </div>
</section>
</div>




<script>
$("#product_carousel").owlCarousel({

    navigation: true, // Show next and prev buttons

    slideSpeed: 300,
    paginationSpeed: 400,

    items: 1,
    itemsDesktop: false,
    itemsDesktopSmall: false,
    itemsTablet: false,
    itemsMobile: false

});
</script>

<?php
include ("footer.php");
?>