<?php
include ("header.php");
?>			<section class="intro_section page_mainslider ds">
				<div class="flexslider" data-dots="false">
					<ul class="slides">

						<li class="flex-active-slide" style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 1; display: block; z-index: 2;">
							<img src="files/slide01.jpg" alt="" draggable="false">
							<div class="container">
								<div class="row">
									<div class="col-sm-12 text-center">
										<div class="slide_description_wrapper">
											<div class="slide_description">
												<div class="intro-layer animated fadeInRight" data-animation="fadeInRight" style="visibility: hidden;">
													<h2>
													Top quality products</h2>
												</div>
												<div class="intro-layer animated fadeInRight" data-animation="fadeInRight" style="visibility: hidden;">
													<h3 class="text-uppercase">WE HOLD QUALITY AND TRACEABILITY CERTIFICATES.

.</h3>
												</div>
											</div>
											<!-- eof .slide_description -->
										</div>
										<!-- eof .slide_description_wrapper -->
									</div>
									<!-- eof .col-* -->
								</div>
								<!-- eof .row -->
							</div>
							<!-- eof .container -->
						</li>

						<li style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;">
							<img src="files/slide02.jpg" alt="" draggable="false">
							<div class="container">
								<div class="row">
									<div class="col-sm-12 text-center">
										<div class="slide_description_wrapper">
											<div class="slide_description">
												<div class="intro-layer" data-animation="fadeInRight" style="visibility: hidden;">
													<h2 class="small">
														Personalised attention													</h2>
												</div>
												<div class="intro-layer" data-animation="fadeInRight" style="visibility: hidden;">
													<!--<h3 class="highlight text-uppercase bottommargin">-->
													<h3 class="text-uppercase">DIRECT CONTACT WITH OUR CUSTOMERS..
</h3>
												</div>
												<!--
												<div class="intro-layer" data-animation="fadeInRight">
													<p>We can solve your corporate IT disposition needs quickly and professionally.<br> Save Your community, Save Your planet</p>
													<a href="gallery-extended-2-cols.html" class="theme_button color1">Our services</a>
												</div>
												-->
											</div>
											<!-- eof .slide_description -->
										</div>
										<!-- eof .slide_description_wrapper -->
									</div>
									<!-- eof .col-* -->
								</div>
								<!-- eof .row -->
							</div>
							<!-- eof .container -->
						</li>

						<li style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;">
							<img src="files/slide03.jpg" alt="" draggable="false">
							<div class="container">
								<div class="row">
									<div class="col-sm-12 text-center">
										<div class="slide_description_wrapper">
											<div class="slide_description">
												<div class="intro-layer" data-animation="fadeInRight" style="visibility: hidden;">
													<h2 class="small">
Service de qualité
												</div>
												<div class="intro-layer" data-animation="fadeInRight" style="visibility: hidden;">
													<h3 class="text-uppercase">THE MOST TECHNICALLY AND FINANCIALLY EFFECTIVE SOLUTION.
</h3>
												</div>
												<!--
												<div class="intro-layer" data-animation="fadeInRight">
													<p>We can solve your corporate IT disposition needs quickly and professionally.<br> Save Your community, Save Your planet</p>
													<a href="gallery-extended-2-cols.html" class="theme_button color1">contact us</a>
												</div>
												-->
											</div>
											<!-- eof .slide_description -->
										</div>
										<!-- eof .slide_description_wrapper -->
									</div>
									<!-- eof .col-* -->
								</div>
								<!-- eof .row -->
							</div>
							<!-- eof .container -->
						</li>

						<li style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;">
							<img src="files/slide04.jpg" alt="" draggable="false">
							<div class="container">
								<div class="row">
									<div class="col-sm-12 text-center">
										<div class="slide_description_wrapper">
											<div class="slide_description">
												<div class="intro-layer" data-animation="fadeInRight" style="visibility: hidden;">
													<h2 class="small">
														Prestataires prestigieux
													</h2>
												</div>
												<div class="intro-layer" data-animation="fadeInRight" style="visibility: hidden;">
													<h3 class="text-uppercase">A SOUND R&D POLICY IS A GUARANTEE OF INNOVATION AND QUALITY.
</h3>
												</div>
												<!--
												<div class="intro-layer" data-animation="fadeInRight">
													<p>We can solve your corporate IT disposition needs quickly and professionally.<br> Save Your community, Save Your planet</p>
													<a href="gallery-extended-2-cols.html" class="theme_button color1">contact us</a>
												</div>
												-->
											</div>
											<!-- eof .slide_description -->
										</div>
										<!-- eof .slide_description_wrapper -->
									</div>
									<!-- eof .col-* -->
								</div>
								<!-- eof .row -->
							</div>
							<!-- eof .container -->
						</li>

					</ul>
				<ul class="flex-direction-nav"><li class="flex-nav-prev"><a class="flex-prev" href="//?lang=en#"></a></li><li class="flex-nav-next"><a class="flex-next" href="//?lang=en#"></a></li></ul></div>
				<!-- eof flexslider -->

				<div class="scroll_button_wrap">
					<a href="//?lang=en#about" class="scroll_button">
						<span class="sr-only">scroll down</span>
					</a>
				</div>
			</section>

			
			
			
<section id="about" class="ls section_padding_top_25 section_padding_bottom_25">
		<div class="container">
			<div class="row">
				<div class="col-sm-10 col-sm-offset-1 col-lg-12  text-left">
					<h3>who are we</h3>
					<!--<h2>INGREDIS TUNISIE. Quality assurance</h2>-->
					<p>INGREDIS TUNISIE SARL is a company specialized in the distribution of ingredients, additives and various functional chemicals for the local agri-food industries.To this end, INGREDIS TUNISIE SARL operates in Tunisia as an exclusive distributor of its raw materials and regularly provides its customers with quality products according to international standards at the best possible price:</p>
<ul>
<li>ENZYMES</li>
<li>EMULSIFIERS</li>
<li>LECITHINS</li>
<li>ANTIOXIDANTS</li>
<li>RELEASE OILS</li>
<li>ADDITIVES, INGREDIENTS,</li>
<li>CHEMICAL PRODUCTS</li>
</ul>
				</div>
			</div>
		</div>
	</section>


	<section class="ls section_padding_bottom_100">
		<div class="container">
			<div class="row topmargin_30 columns_margin_bottom_20">

				<div class="col-md-4 col-sm-6">
					<article class="vertical-item content-padding with_background text-center rounded overflow-hidden">
						<div class="item-media">
							<img src="files/image_161.jpg" alt="">
						</div>
						<div class="item-content">
							<h3 class="entry-title"> <!-- -->
								 SOURCING

					</h3>
							<p class="margin_0">
															</p>
							<!--<a href="service-single.html" class="read-more"></a>-->
						</div>
					</article>
				</div>

				<div class="col-md-4 col-sm-6">
					<article class="vertical-item content-padding with_background text-center rounded overflow-hidden">
						<div class="item-media">
							<img src="files/image_162.jpg" alt="">
						</div>
						<div class="item-content">
							<h3 class="entry-title"> <!-- class="entry-title" -->
								STORAGE				</h3>
							<p class="margin_0">
															</p>
							<!--<a href="service-single.html" class="read-more"></a>-->
						</div>
					</article>
				</div>

				<div class="col-md-4 col-sm-6">
					<article class="vertical-item content-padding with_background text-center rounded overflow-hidden">
						<div class="item-media">
							<img src="files/image_163.jpg" alt="">
						</div>
						<div class="item-content" style="max-height:265px">
							<h3 class="entry-title"> <!-- class="entry-title" -->
								
 DISTRIBUTION

					</h3>
							
							<!--<a href="service-single.html" class="read-more"></a>-->
						</div>
					</article>
				</div>
				<div class="col-sm-10 col-sm-offset-1 col-lg-12  text-left">
<p>Our principle is to regularly insure our raw materials in stock in our store.
 We thus allow our customers a considerable solution: immediate availability. 
 Because we attach great importance to the availability of raw materials, the location of sources of supply,
 Purchase forecasts from our regular customers.
<br>
For INGREDIS TUNISIA, logistics summarized in three main axes: import, storage 
and delivery is a major strategic issue. We consider it a real source of added value 
customers in the form of quality of service, on-time performance and responsiveness. In addition, logistics is one of the 
key locations where the profitability of our company is at stake, through the optimization of communication and information to
 day on inventory status and transportation costs.
<br>
We also rely on the logistics department of our European client, which specializes in international trade.
 Whether it’s FCL or LCL, it oversees sales or purchases around the world, controls quality at the origin, handles customs procedures... 
 This department is also specialized in incoterms, documentary credits, maritime insurance, triangular operations and 
 other activities extending logistical activities.
 

</p></div>
				
			</div>
		</div>
	</section>
			
			
			<section id="about" class="ls section_padding_top_100 section_padding_bottom_50">
				<div class="container">
					<div class="row">
						<div class="col-sm-10 col-sm-offset-1 col-lg-8 col-lg-offset-2 text-center">
							<h2 class="section_header">
							Raw materials for the food industry
							</h2>
							<!--<p class="small-text grey">More About Us</p>-->
							<p class="bottommargin_30">
							We supply premium ingredients and additives for the food industry.

								<!--Suministramos ingredientes y aditivos de la más alta calidad para la industria alimentaria.-->
							</p>
							<!--<img src="images/signature.png" alt="" />-->
						</div>
					</div>

					<div class="row topmargin_40 columns_margin_top_60">

						<div class="col-md-4">
							<div class="teaser with_border rounded text-center">
								<div class="teaser_icon main_bg_color2 round size_small offset_icon">
									<i class="rt-icon2-leaf"></i>
								</div>
								<h4 class="poppins hover-color2">
									<a href="#">Personalized attention</a>
								</h4>
								<p>
									Cela nous permet de déterminer le dialogue approprié avec nos clients et nos clients.						</p>
							</div>
						</div>
						<div class="col-md-4">
							<div class="teaser with_border rounded text-center">
								<div class="teaser_icon main_bg_color3 round size_small offset_icon">
									<i class="rt-icon2-paper"></i>
								</div>
								<h4 class="poppins hover-color3">
									<a href="#">Market segments</a>
								</h4>
								<p>
								Our customers are always in close contact with a professional who knows their industry well.							</div>
						</div>
						<div class="col-md-4">
							<div class="teaser with_border rounded text-center">
								<div class="teaser_icon main_bg_color round size_small offset_icon">
									<i class="rt-icon2-shopping-cart"></i>
								</div>
								<h4 class="poppins">
									<a href="#">quality products</a>
								</h4>
								<p>
								We offer a wide range of products to cover all food sectors.							</p>
							</div>
						</div>
					</div>
				</div>
			</section>

			
			<section id="services" class="ls ms section_padding_50">
				<div class="container">
					<div class="row">
						<div class="col-sm-12 text-center">
							<h2 class="section_header">
								Assurance qualité
						</h2>
							<p class="small-text grey">
							ALL OUR PRODUCTS ARE CERTIFIED ACCORDING TO A QUALITY MANAGEMENT SYSTEM BASED ON DIFFERENT STANDARDS AS A PRODUCT LINE. THE AIM OF THIS SYSTEM IS TO DEFINE AND CONTROL THE WORK PROCESSES IN ORDER TO GUARANTEE THE QUALITY OF OUR SERVICES AND PRODUCTS.							</p>
						</div>
					</div>
									</div>
			</section>
			
			<?php
include ("footer.php");
?>