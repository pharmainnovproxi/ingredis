<?php
include ("header.php");
?>	
	<section class="ls section_padding_top_25 section_padding_bottom_150">
		<div class="container">
			<div class="row columns_padding_25 columns_margin_bottom_20">
				<div class="col-md-4">
					<img src="files/panaderia.jpg" width="528" alt="">
				</div>
				<div class="col-md-8">
					<h2>
						Bienvenue à Ingredis Tunisie
				</h2>
					<p>
						Ingredis Tunisie a été créée en octobre 1997 par des professionnels possédant une vaste expérience dans le secteur alimentaire. Elle a commencé ses activités en 1998 en tant que distributeur et agent pour la vente d'ingrédients et d'additifs pour l'industrie alimentaire.		</p>
					<p>
					L'objectif de l'entreprise est d'offrir un service technique et commercial dynamique et efficace afin d'assurer la satisfaction et la confiance de nos clients. Pour ce faire, nous avons une équipe de professionnels qualifiés avec une vaste expérience dans le secteur et la formation spécifique pour fournir une solution personnalisée pour chaque cas.			</p>
					<p>
Depuis le tout début, nous avons travaillé avec un groupe diversifié de fournisseurs leaders de l'industrie avec des départements de R&amp;D développant et fournissant à l'industrie alimentaire une gamme de solutions nouvelles et innovantes aux défis rencontrés. Aujourd'hui, nous avons des fournisseurs en Europe, en Amérique et en Asie				</p>
				</div>
			</div>
<!--
			<div class="row">
				
				<h2>
					Pourquoi Ingredis Tunisie?				</h2>

				<div class="col-sm-6">
					<div class="panel-group" id="accordion1">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a data-toggle="collapse" data-parent="#accordion1" href="societe.php#collapse1">
										Grâce à notre équipe								</a>
								</h4>
							</div>
							<div id="collapse1" class="panel-collapse collapse in">
								<div class="panel-body">
									Notre équipe est formée de <strong>professionnels avec une vaste expérience</strong> dans les différents secteurs de l'industrie alimentaire et avec les connaissances nécessaires pour trouver la meilleure solution technique pour chaque application. En raison de la nature exigeante de notre marché, cette équipe est <strong>entièrement formée de diplômés universitaires</strong> spécialisés dans les différentes branches de l'industrie. De cette façon, nous pouvons garantir un dialogue approprié avec nos clients et clients.			</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a data-toggle="collapse" data-parent="#accordion1" href="societe.php#collapse2" class="collapsed">
										En raison de la qualité de nos produits							</a>
								</h4>
							</div>
							<div id="collapse2" class="panel-collapse collapse">
								<div class="panel-body">
							Une gamme de produits fabriqués par des entreprises internationales dont les <strong>normes technologiques élevées et la capacité de production sont la meilleure garantie possible</strong>. Tous ces produits bénéficient d'une <strong>certification de qualité et de traçabilité</strong> via les documents correspondants et apportent à nos clients des <strong>solutions techniques et économiques</strong> pour tout processus industriel. 			</div>
							</div>
						</div>
					</div>
				</div>

				<div class="col-sm-6">
					<div class="panel-group" id="accordion2">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a data-toggle="collapse" data-parent="#accordion2" href="societe.php#collapse3" class="" aria-expanded="true">
										Grâce à une structure efficace								</a>
								</h4>
							</div>
							<div id="collapse3" class="panel-collapse collapse in" aria-expanded="true" style="">
								<div class="panel-body">
						Une <strong>organisation administrative rapide et fiable</strong>, toujours prête à résoudre les problèmes des clients de la meilleure façon et dans les plus brefs délais.							</div>
							</div>
						</div>

						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a data-toggle="collapse" data-parent="#accordion2" href="societe.php#collapse4" class="collapsed" aria-expanded="false">
										Grâce à un service logistique fiable								</a>
								</h4>
							</div>
							<div id="collapse4" class="panel-collapse collapse" aria-expanded="false">
								<div class="panel-body">
									Un système logistique qui comprend <strong>le stockage, la livraison et le transport</strong> et qui offre une <strong>solution fiable et efficace</strong> selon nos besoins et ceux de nos clients.							</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>-->
	</section>

<?php
include ("footer.php");
?>	