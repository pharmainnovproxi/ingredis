<?php
include ("header.php");
?>			<section class="intro_section page_mainslider ds">
				<div class="flexslider" data-dots="false">
					<ul class="slides">

						<li class="flex-active-slide" style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 1; display: block; z-index: 2;">
							<img src="files/slide01.jpg" alt="" draggable="false">
							<div class="container">
								<div class="row">
									<div class="col-sm-12 text-center">
										<div class="slide_description_wrapper">
											<div class="slide_description">
												<div class="intro-layer animated fadeInRight" data-animation="fadeInRight" style="visibility: hidden;">
													<h2>
														Produits de qualité supérieure											</h2>
												</div>
												<div class="intro-layer animated fadeInRight" data-animation="fadeInRight" style="visibility: hidden;">
													<h3 class="text-uppercase">NOUS DÉTENONS DES CERTIFICATS DE QUALITÉ ET DE TRAÇABILITÉ.
.</h3>
												</div>
											</div>
											<!-- eof .slide_description -->
										</div>
										<!-- eof .slide_description_wrapper -->
									</div>
									<!-- eof .col-* -->
								</div>
								<!-- eof .row -->
							</div>
							<!-- eof .container -->
						</li>

						<li style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;">
							<img src="files/slide02.jpg" alt="" draggable="false">
							<div class="container">
								<div class="row">
									<div class="col-sm-12 text-center">
										<div class="slide_description_wrapper">
											<div class="slide_description">
												<div class="intro-layer" data-animation="fadeInRight" style="visibility: hidden;">
													<h2 class="small">
														Personalised attention													</h2>
												</div>
												<div class="intro-layer" data-animation="fadeInRight" style="visibility: hidden;">
													<!--<h3 class="highlight text-uppercase bottommargin">-->
													<h3 class="text-uppercase">CONTACT DIRECT AVEC NOS CLIENTS.
</h3>
												</div>
												<!--
												<div class="intro-layer" data-animation="fadeInRight">
													<p>We can solve your corporate IT disposition needs quickly and professionally.<br> Save Your community, Save Your planet</p>
													<a href="gallery-extended-2-cols.html" class="theme_button color1">Our services</a>
												</div>
												-->
											</div>
											<!-- eof .slide_description -->
										</div>
										<!-- eof .slide_description_wrapper -->
									</div>
									<!-- eof .col-* -->
								</div>
								<!-- eof .row -->
							</div>
							<!-- eof .container -->
						</li>

						<li style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;">
							<img src="files/slide03.jpg" alt="" draggable="false">
							<div class="container">
								<div class="row">
									<div class="col-sm-12 text-center">
										<div class="slide_description_wrapper">
											<div class="slide_description">
												<div class="intro-layer" data-animation="fadeInRight" style="visibility: hidden;">
													<h2 class="small">
Service de qualité
												</div>
												<div class="intro-layer" data-animation="fadeInRight" style="visibility: hidden;">
													<h3 class="text-uppercase">LA SOLUTION LA PLUS EFFICACE SUR LE PLAN TECHNIQUE ET FINANCIER.
</h3>
												</div>
												<!--
												<div class="intro-layer" data-animation="fadeInRight">
													<p>We can solve your corporate IT disposition needs quickly and professionally.<br> Save Your community, Save Your planet</p>
													<a href="gallery-extended-2-cols.html" class="theme_button color1">contact us</a>
												</div>
												-->
											</div>
											<!-- eof .slide_description -->
										</div>
										<!-- eof .slide_description_wrapper -->
									</div>
									<!-- eof .col-* -->
								</div>
								<!-- eof .row -->
							</div>
							<!-- eof .container -->
						</li>

						<li style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;">
							<img src="files/slide04.jpg" alt="" draggable="false">
							<div class="container">
								<div class="row">
									<div class="col-sm-12 text-center">
										<div class="slide_description_wrapper">
											<div class="slide_description">
												<div class="intro-layer" data-animation="fadeInRight" style="visibility: hidden;">
													<h2 class="small">
														Prestataires prestigieux
													</h2>
												</div>
												<div class="intro-layer" data-animation="fadeInRight" style="visibility: hidden;">
													<h3 class="text-uppercase">UNE POLITIQUE R&D SOLIDE EST UN GAGE D'INNOVATION ET DE QUALITÉ.
</h3>
												</div>
												<!--
												<div class="intro-layer" data-animation="fadeInRight">
													<p>We can solve your corporate IT disposition needs quickly and professionally.<br> Save Your community, Save Your planet</p>
													<a href="gallery-extended-2-cols.html" class="theme_button color1">contact us</a>
												</div>
												-->
											</div>
											<!-- eof .slide_description -->
										</div>
										<!-- eof .slide_description_wrapper -->
									</div>
									<!-- eof .col-* -->
								</div>
								<!-- eof .row -->
							</div>
							<!-- eof .container -->
						</li>

					</ul>
				<ul class="flex-direction-nav"><li class="flex-nav-prev"><a class="flex-prev" href="//?lang=en#"></a></li><li class="flex-nav-next"><a class="flex-next" href="//?lang=en#"></a></li></ul></div>
				<!-- eof flexslider -->

				<div class="scroll_button_wrap">
					<a href="//?lang=en#about" class="scroll_button">
						<span class="sr-only">scroll down</span>
					</a>
				</div>
			</section>

			
			
			
<section id="about" class="ls section_padding_top_25 section_padding_bottom_25">
		<div class="container">
			<div class="row">
				<div class="col-sm-10 col-sm-offset-1 col-lg-12  text-left">
					<h3>QUI SOMMES NOUS</h3>
					<!--<h2>INGREDIS TUNISIE. Quality assurance</h2>-->
					<p>INGREDIS TUNISIE SARL est une société spécialisée dans la distribution des ingrédients, additifs et différents produits chimiques fonctionnels pour les industries agroalimentaires locales. A cet effet, INGREDIS TUNISIE SARL intervient sur le territoire tunisien en tant que distributeur exclusif des ses matières premières et fournit régulièrement,
					à ses clients, des produits de qualité selon les normes internationales au meilleur prix possible :</p>
<ul>
<li>ENZYMES</li>
<li>ÉMULSIFIANTS</li>
<li>LÉCITHINES</li>
<li>ANTIOXYDANTS</li>
<li>HUILES DE DÉMOULAGE</li>
<li>ADDITIFS, INGRÉDIENTS,</li>
<li>PRODUITS CHIMIQUES</li>
</ul>
				</div>
			</div>
		</div>
	</section>


	<section class="ls section_padding_bottom_100">
		<div class="container">
			<div class="row topmargin_30 columns_margin_bottom_20">

				<div class="col-md-4 col-sm-6">
					<article class="vertical-item content-padding with_background text-center rounded overflow-hidden">
						<div class="item-media">
							<img src="files/image_161.jpg" alt="">
						</div>
						<div class="item-content">
							<h3 class="entry-title"> <!-- -->
								 SOURCING

					</h3>
							<p class="margin_0">
															</p>
							<!--<a href="service-single.html" class="read-more"></a>-->
						</div>
					</article>
				</div>

				<div class="col-md-4 col-sm-6">
					<article class="vertical-item content-padding with_background text-center rounded overflow-hidden">
						<div class="item-media">
							<img src="files/image_162.jpg" alt="">
						</div>
						<div class="item-content">
							<h3 class="entry-title"> <!-- class="entry-title" -->
								ENTREPOSAGE					</h3>
							<p class="margin_0">
															</p>
							<!--<a href="service-single.html" class="read-more"></a>-->
						</div>
					</article>
				</div>

				<div class="col-md-4 col-sm-6">
					<article class="vertical-item content-padding with_background text-center rounded overflow-hidden">
						<div class="item-media">
							<img src="files/image_163.jpg" alt="">
						</div>
						<div class="item-content" style="max-height:265px">
							<h3 class="entry-title"> <!-- class="entry-title" -->
								
 DISTRIBUTION

					</h3>
							
							<!--<a href="service-single.html" class="read-more"></a>-->
						</div>
					</article>
				</div>
				<div class="col-sm-10 col-sm-offset-1 col-lg-12  text-left">
<p>Notre principe, c'est d'assurer régulièrement nos matières premières en stock dans notre magasin.
 Nous permettons ainsi à nos clients une solution considérable : une disponibilité immédiate. 
 Car nous accordons beaucoup d’importance à la disponibilité en matières premières, la localisation des sources d’approvisionnement,
 les prévisions d’achats de nos clients réguliers.
<br>
Pour INGREDIS TUNISIE, la logistique résumée en trois axes principaux : l'import, le stockage 
et la livraison est un enjeu stratégique majeur. Nous la considérons comme un véritable gisement de valeur ajoutée auprès 
des clients sous forme de qualité de service, de performance en délai et en réactivité. En outre, la logistique est un des 
lieux principaux où se joue la rentabilité de notre entreprise, par l’optimisation de la communication et l’information à
 jour sur l’état des stocks et des coûts de transport.
<br>
Nous comptons aussi sur le département logistique de notre commettant européen, spécialisé dans le commerce international.
 Que ce soit FCL ou LCL, il supervise les ventes ou achats partout dans le monde, contrôle la qualité à l'origine, s’occupe des procédures douanières… 
 Ce département est aussi spécialiste dans les les incoterms, les crédits documentaires, les assurances maritimes, les opérations triangulaires et 
 d’autres activités prolongeant les activités logistiques.

 

</p></div>
				
			</div>
		</div>
	</section>
			
			
			<section id="about" class="ls section_padding_top_100 section_padding_bottom_50">
				<div class="container">
					<div class="row">
						<div class="col-sm-10 col-sm-offset-1 col-lg-8 col-lg-offset-2 text-center">
							<h2 class="section_header">
								Matières premières pour l'industrie agroalimentaire
							</h2>
							<!--<p class="small-text grey">More About Us</p>-->
							<p class="bottommargin_30">
								Nous fournissons des ingrédients et des additifs de première qualité pour l'industrie alimentaire.

								<!--Suministramos ingredientes y aditivos de la más alta calidad para la industria alimentaria.-->
							</p>
							<!--<img src="images/signature.png" alt="" />-->
						</div>
					</div>

					<div class="row topmargin_40 columns_margin_top_60">

						<div class="col-md-4">
							<div class="teaser with_border rounded text-center">
								<div class="teaser_icon main_bg_color2 round size_small offset_icon">
									<i class="rt-icon2-leaf"></i>
								</div>
								<h4 class="poppins hover-color2">
									<a href="#">Attention personnalisée</a>
								</h4>
								<p>
									Cela nous permet de déterminer le dialogue approprié avec nos clients et nos clients.						</p>
							</div>
						</div>
						<div class="col-md-4">
							<div class="teaser with_border rounded text-center">
								<div class="teaser_icon main_bg_color3 round size_small offset_icon">
									<i class="rt-icon2-paper"></i>
								</div>
								<h4 class="poppins hover-color3">
									<a href="#">Les segments du marché</a>
								</h4>
								<p>
									Nos clients sont toujours en contact étroit avec un professionnel qui connaît bien leur industrie.								</p>
							</div>
						</div>
						<div class="col-md-4">
							<div class="teaser with_border rounded text-center">
								<div class="teaser_icon main_bg_color round size_small offset_icon">
									<i class="rt-icon2-shopping-cart"></i>
								</div>
								<h4 class="poppins">
									<a href="#">Produits de qualité</a>
								</h4>
								<p>
									Nous proposons une large gamme de produits pour couvrir tous les secteurs alimentaires.							</p>
							</div>
						</div>
					</div>
				</div>
			</section>

			
			<section id="services" class="ls ms section_padding_50">
				<div class="container">
					<div class="row">
						<div class="col-sm-12 text-center">
							<h2 class="section_header">
								Assurance qualité
						</h2>
							<p class="small-text grey">
								TOUS NOS PRODUITS SONT CERTIFIÉS SELON UN SYSTÈME DE GESTION DE LA QUALITÉ BASÉ SUR DIFFÉRENTES NORMES EN TANT QUE GAMME DE PRODUITS. L'OBJECTIF DE CE SYSTÈME EST DE BIEN DÉFINIR ET MAÎTRISER LES PROCESSUS DE TRAVAIL AFIN DE GARANTIR LA QUALITÉ DE NOS SERVICES ET PRODUITS.							</p>
						</div>
					</div>
									</div>
			</section>
			
			<?php
include ("footer.php");
?>