<?php
include ("header.php");
?>

<section id="about" class="ls section_padding_top_25">

    <div class="container">
        <div class="row">

            <div class="col-sm-7 col-lg-7  text-left">
                <h3> ÉMULSIFIANTS SYNTHÉTIQUES (ESTERS)</h3>
                <p>Les émulsifiants sont un ingrédient clé dans la fabrication des améliorants de panification de
                    qualité, mais ils sont également utilisés dans de nombreux autres domaines de l'industrie
                    alimentaire. Les émulsifiants affectent l'apparence, la texture et la structure, tout en aidant à
                    maintenir la qualité et la fraîcheur.
                </p>
                <p>Nos émulsifiants utilisent de l'huile végétale de haute qualité, de l'huile de palme ou de l'huile de
                    colza comme matières premières principales.
                </p>
                <p>Des mono- et di-glycérides aux esters plus sophistiqués, notre partenaire exclusif a développé toute
                    une gamme de dérivés d'acides gras : purs, co-pulvérisés pour de nombreux secteurs de l'industrie
                    alimentaire. En plus des émulsifiants pour la boulangerie, nous fournissons de nombreux autres
                    segments tels que les émulsifiants pour la fabrication du "cake gel improver", les produits
                    laitiers, la margarine, les sauces, les flocons de pommes de terre, la charcuterie...</p>
            </div>

            <div class="col-sm-5 col-lg-5  text-left">
                <div class="owl-carousel owl-theme" id="product_carousel">
                    <div class="item">
                        <article class="vertical-item content-padding post format-standard with_background rounded">
                            <div class="item-media">
                                <img src="files/img/ESTERS/1P.jpg" alt="">
                                <div class="media-links">
                                    <div class="links-wrap">
                                        <a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]"
                                            href="files/img/ESTERS/1P.jpg"></a>
                                    </div>
                                </div>
                            </div>

                        </article>
                    </div>
                    <div class="item">
                        <article class="vertical-item content-padding post format-standard with_background rounded">
                            <div class="item-media">
                                <img src="files/img/ESTERS/2P.jpg" alt="">
                                <div class="media-links">
                                    <div class="links-wrap">
                                        <a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]"
                                            href="files/img/ESTERS/2P.jpg"></a>
                                    </div>
                                </div>
                            </div>

                        </article>
                    </div>
                    <div class="item">
                        <article class="vertical-item content-padding post format-standard with_background rounded">
                            <div class="item-media">
                                <img src="files/img/ESTERS/3P.jpg" alt="">
                                <div class="media-links">
                                    <div class="links-wrap">
                                        <a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]"
                                            href="files/img/ESTERS/3P.jpg"></a>
                                    </div>
                                </div>
                            </div>

                        </article>
                    </div>
                    <div class="item">
                        <article class="vertical-item content-padding post format-standard with_background rounded">
                            <div class="item-media">
                                <img src="files/img/ESTERS/4P.png" alt="">
                                <div class="media-links">
                                    <div class="links-wrap">
                                        <a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]"
                                            href="files/img/ESTERS/4P.png"></a>
                                    </div>
                                </div>
                            </div>

                        </article>
                    </div>
                    <div class="item">
                        <article class="vertical-item content-padding post format-standard with_background rounded">
                            <div class="item-media">
                                <img src="files/img/ESTERS/5P.jpg" alt="">
                                <div class="media-links">
                                    <div class="links-wrap">
                                        <a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]"
                                            href="files/img/ESTERS/5P.jpg"></a>
                                    </div>
                                </div>
                            </div>

                        </article>
                    </div>

                </div>

            </div>

        </div>
        <div class="row" style="margin-bottom:50px;">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <p>
                    <strong>
                    Le portefeuille d'émulsifiants INGREDIS TUNISIE comprend :
                    </strong>
                <ul>
                    <li>E471 monoglycérides à 40%, 60% et distillés à 90%</li>
                    <li>E475 (Polyglycerol esters)</li>
                    <li>E476 (PGPR)</li>
                    <li>E472a, E472b, E472c, E472e</li>
                    <li>E491, E492 (Sorbitan stearate) </li>
                    <li>Agents liants et cristallisants (Oil binders & Crystal promoters)</li>
                    <li>Emulsifiants activés en poudre (activated emulsifier systems)</li>
                    <li>...</li>

                </ul>
                </p>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">

                <strong>
                INGREDIS TUNISIE est une référence en tant que distributeur d'émulsifiants en offrant une large gamme pour différentes applications.:
                </strong>
                <ul>
                    <li>
                    Boulangerie
                    </li>
                    <li> Gâteaux et biscuits</li>
                    <li>Chocolat et confiserie</li>
                    <li>Glaces et produits laitiers</li>
                    <li>Margarines et tartinades</li>
                    <li>Charcuterie</li>
                    <li>Minoterie</li>
                    <li>Production de chips</li>
                    <li>Snacks et assaisonnements</li>
                    <li>Alimentation animale.</li>

                </ul>
            </div>
        </div>
    </div>
</section>
</div>




<script>
$("#product_carousel").owlCarousel({

    navigation: true, // Show next and prev buttons

    slideSpeed: 300,
    paginationSpeed: 400,

    items: 1,
    itemsDesktop: false,
    itemsDesktopSmall: false,
    itemsTablet: false,
    itemsMobile: false

});
</script>

<?php
include ("footer.php");
?>