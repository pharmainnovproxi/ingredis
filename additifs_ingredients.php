<?php
include ("header.php");
?>

<section id="about" class="ls section_padding_top_25">

    <div class="container">
        <div class="row">

            <div class="col-sm-7 col-lg-7  text-left">
                <h3> ADDITIFS ET INGRÉDIENTS</h3>
                <p>La vocation de INGREDIS TUNISIE a toujours été d’agir non seulement en tant que fournisseur de matières premières, mais également comme un partenaire réel de nos clients, en offrant des solutions concrètes du point de vue technique dans le domaine des industries agroalimentaires.  :
                </p>
                <ul>
                    <li>Collaboration dans des projets de R&D avec des entreprises et des centres techniques. </li>
                    <li>Développement et fabrication de formulations sur mesure.</li>
                    <li>Conseil technique sur les additifs et la législation.</li>
                    <li>Conseil technique sur les process de production.</li>
                </ul>
            </div>

            <div class="col-sm-5 col-lg-5  text-left">
                <div class="owl-carousel owl-theme" id="product_carousel">
                    <div class="item">
                        <article class="vertical-item content-padding post format-standard with_background rounded">
                            <div class="item-media">
                                <img src="files/img/ADDITIFS_INGREDIENTS/1P.jpeg" alt="">
                                <div class="media-links">
                                    <div class="links-wrap">
                                        <a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]"
                                            href="files/img/ADDITIFS_INGREDIENTS/1P.jpeg"></a>
                                    </div>
                                </div>
                            </div>

                        </article>
                    </div>
                    <div class="item">
                        <article class="vertical-item content-padding post format-standard with_background rounded">
                            <div class="item-media">
                                <img src="files/img/ADDITIFS_INGREDIENTS/2P.jpg" alt="">
                                <div class="media-links">
                                    <div class="links-wrap">
                                        <a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]"
                                            href="files/img/ADDITIFS_INGREDIENTS/2P.jpg"></a>
                                    </div>
                                </div>
                            </div>

                        </article>
                    </div>
                    <div class="item">
                        <article class="vertical-item content-padding post format-standard with_background rounded">
                            <div class="item-media">
                                <img src="files/img/ADDITIFS_INGREDIENTS/3P.webp" alt="">
                                <div class="media-links">
                                    <div class="links-wrap">
                                        <a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]"
                                            href="files/img/ADDITIFS_INGREDIENTS/3P.webp"></a>
                                    </div>
                                </div>
                            </div>

                        </article>
                    </div>
                    <div class="item">
                        <article class="vertical-item content-padding post format-standard with_background rounded">
                            <div class="item-media">
                                <img src="files/img/ADDITIFS_INGREDIENTS/4P.png" alt="">
                                <div class="media-links">
                                    <div class="links-wrap">
                                        <a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]"
                                            href="files/img/ADDITIFS_INGREDIENTS/4P.png"></a>
                                    </div>
                                </div>
                            </div>

                        </article>
                    </div>
                    <div class="item">
                        <article class="vertical-item content-padding post format-standard with_background rounded">
                            <div class="item-media">
                                <img src="files/img/ADDITIFS_INGREDIENTS/5P.jpg" alt="">
                                <div class="media-links">
                                    <div class="links-wrap">
                                        <a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]"
                                            href="files/img/ADDITIFS_INGREDIENTS/5P.jpg"></a>
                                    </div>
                                </div>
                            </div>

                        </article>
                    </div>

                </div>

            </div>

        </div>
        <div class="row">
            <div class="col-12">
                <p>Nous offrons pour ce faire toute une gamme d'additifs et ingrédients pour la formulation de produits agroalimentaires :</p>
            </div>
        </div>
        <div class="row" style="margin-bottom:50px;">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <p>
                    <strong>
                    ANTI-OXYDANTS 
                    </strong>
                <ul>
                    <li>EDTA (E385) (Ca, Na)</li>
                    <li>EDTA (E385) LIQUIDE</li>
                    <li>BHA (E320)</li>
                    <li>BHT (E321)</li>
                    <li>Propyl gallate (E310)</li>
                    <li>TBHQ (E319)</li>
                    <li>...</li>
                </ul>
                </p>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">

                <strong>
                ADDITIFS & INGREDIENTS DIVERS :
                </strong>
                <ul>
                    <li>
                    Acide ascorbique (E300)
                    </li>
                    <li>Acide citrique (E330)</li>
                    <li>Acide lactique (E270)</li>
                    <li>Acide phosphorique (E338) </li>
                    <li>Amidon modifié (E1422)</li>
                    <li>Benzoate de sodium (E211)</li>
                    <li>Chlorure de calcium (E509)</li>
                    <li>Citrate de sodium (E331)</li>
                    <li>CMC Carboxyméthylcellulose (E466)</li>
                    <li>Extrait de levures (exhausteur de goût )</li>
                    <li>Extrait de Malt</li>
                    <li>Fibres (bambou, canne à sucre, blé...)</li>
                    <li>Gomme guar (E412)</li>
                    <li>Gomme xanthane (E415)</li>
                    <li>Légumes déshydratés en poudre</li>
                    <li>Maltodextrine</li>
                    <li>Métabisulfite de sodium (E223)</li>
                    <li>Protéines de soja</li>
                    <li>Pyrophosphates (SAPP 28)</li>
                    <li>Sorbate de potassium (E202)</li>
                    <li>Tri polyphosphates (STPP)</li>
                    <li>...</li>
                </ul>
            </div>
        </div>
    </div>
</section>
</div>




<script>
$("#product_carousel").owlCarousel({

    navigation: true, // Show next and prev buttons

    slideSpeed: 300,
    paginationSpeed: 400,

    items: 1,
    itemsDesktop: false,
    itemsDesktopSmall: false,
    itemsTablet: false,
    itemsMobile: false

});
</script>

<?php
include ("footer.php");
?>