<?php
include ("header.php");
?>

<section id="about" class="ls section_padding_top_25">

    <div class="container">
        <div class="row">

            <div class="col-sm-7 col-lg-7  text-left">
                <h3>ENZYMES</h3>
                <p>Les enzymes sont des protéines qui agissent comme des catalyseurs pour une gamme illimitée de
                    processus biologiques. Ce sont des auxiliaires de traitement hautement spécifiques et constituent
                    des outils très efficaces pour décomposer des molécules complexes en composents élementaires simples
                    ou pour aider à créer ou stabiliser de grandes structures moléculaires. Leur présence dans tous les
                    organismes vivants démontre leur rôle essentiel dans le processus de la vie.
                </p>
                <p>Par ailleurs, la biotechnologie moderne a permis le développement d'enzymes produites par
                    fermentation, en utilisant les dernières techniques d'amélioration des souches pour sélectionner des
                    microorganismes d'origine fongique ou bactérienne capables de produire des enzymes purs et sûrs.
                    Notre partenaire allemand est à la pointe de cette science et a développé une gamme d'enzymes
                    alimentaires et alimentaires polyvalentes.</p>
            </div>

            <div class="col-sm-5 col-lg-5  text-left">
                <div class="owl-carousel owl-theme" id="product_carousel">
                    <div class="item">
                        <article class="vertical-item content-padding post format-standard with_background rounded">
                            <div class="item-media">
                                <img src="files/img/ENZYMES/1P.jpg" alt="">
                                <div class="media-links">
                                    <div class="links-wrap">
                                        <a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]"
                                            href="files/img/ENZYMES/1P.jpg"></a>
                                    </div>
                                </div>
                            </div>

                        </article>
                    </div>
                    <div class="item">
                        <article class="vertical-item content-padding post format-standard with_background rounded">
                            <div class="item-media">
                                <img src="files/img/ENZYMES/2P.jpg" alt="">
                                <div class="media-links">
                                    <div class="links-wrap">
                                        <a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]"
                                            href="files/img/ENZYMES/2P.jpg"></a>
                                    </div>
                                </div>
                            </div>

                        </article>
                    </div>
                    <div class="item">
                        <article class="vertical-item content-padding post format-standard with_background rounded">
                            <div class="item-media">
                                <img src="files/img/ENZYMES/3P.jpg" alt="">
                                <div class="media-links">
                                    <div class="links-wrap">
                                        <a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]"
                                            href="files/img/ENZYMES/3P.jpg"></a>
                                    </div>
                                </div>
                            </div>

                        </article>
                    </div>
                    <div class="item">
                        <article class="vertical-item content-padding post format-standard with_background rounded">
                            <div class="item-media">
                                <img src="files/img/ENZYMES/4P.jpg" alt="">
                                <div class="media-links">
                                    <div class="links-wrap">
                                        <a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]"
                                            href="files/img/ENZYMES/4P.jpg"></a>
                                    </div>
                                </div>
                            </div>

                        </article>
                    </div>
                    <div class="item">
                        <article class="vertical-item content-padding post format-standard with_background rounded">
                            <div class="item-media">
                                <img src="files/img/ENZYMES/5P.webp" alt="">
                                <div class="media-links">
                                    <div class="links-wrap">
                                        <a class="p-view prettyPhoto " title="" data-gal="prettyPhoto[gal]"
                                            href="files/img/ENZYMES/5P.webp"></a>
                                    </div>
                                </div>
                            </div>

                        </article>
                    </div>
                    
                </div>

            </div>

        </div>
        <div class="row" style="margin-bottom:50px;">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <p>
                    <strong>
                        Les principales catégories d’enzymes que nous commercialisons en partenariat exclusif avec notre
                        partenaire Allemand, sont présentées ci-dessous :
                    </strong>
                <ul>
                    <li>Xylanases</li>
                    <li>Alpha amylases</li>
                    <li>Glucose oxydases</li>
                    <li>Lipases</li>
                    <li>Protéases</li>
                    <li>Pectinases</li>
                    <li>...</li>

                </ul>
                </p>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <p>
                    Les enzymes doivent être dosées avec précision, un surdosage entraînant un défaut au niveau du
                    produit. Dans la majorité des cas, les enzymes sont des hydrolases qui dépolymérisent : des
                    protéines, des polymères glucidiques, des polymères de parois végétales et des lipides
                    triglycérique.
                </p>
                <strong>
                    Nos enzymes sont utilisées dans les applications suivantes:
                </strong>
                <ul>
                    <li>Boulangerie : améliorants de panification</li>
                    <li>Gâteaux et biscuits</li>
                    <li>Minoterie : préparation de farines améliorées</li>
                    <li>Alimentation animale</li>


                </ul>
            </div>
        </div>
    </div>
</section>
</div>




<script>
$("#product_carousel").owlCarousel({

    navigation: true, // Show next and prev buttons

    slideSpeed: 300,
    paginationSpeed: 400,

    items: 1,
    itemsDesktop: false,
    itemsDesktopSmall: false,
    itemsTablet: false,
    itemsMobile: false

});
</script>

<?php
include ("footer.php");
?>